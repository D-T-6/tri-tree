#include <raylib.h>

#include <vector>
#include <ostream>

using std::vector;
 
/**
 * === Geometry ===
 */

enum TriState { IS_BEING_CREATED, IS_BEING_DELETED, IDLE };

// Triangle object
// * stores indices of the vertices and a couple other variables
struct Tri {
    int a, b, c;

    int state = IDLE;
    int counter = 0;

    Tri(int _a, int _b, int _c, int _counter, int _state)
        : a(_a), b(_b), c(_c), counter(_counter), state(_state) {}

    int &operator[](int i) {
        if (i == 0) return a;
        if (i == 1) return b;
        return c;
    }
};

// Vertex object
// * Extends Vector2 and adds a vector of integers - indices of parent triangles
struct Vert : Vector2 {
    std::vector<int> tris;

    Vert(float _x, float _y) : Vector2{_x, _y} {}
    void operator=(Vector2 v) {
        x = v.x;
        y = v.y;
    }
};

// Create a vertex from two floats and insert it into the vector
void createVert(vector<Vert> &verts, float x, float y);

// Create a vertex from a vector and insert it into the vector
void createVert(vector<Vert> &verts, Vector2 v);

// Erase a vertex and its parent triangles from the vectors by index
void eraseVert(vector<Vert> &verts, vector<Tri> &tris, int vert);

// Check if given triangle contains a given index
bool triHasVert(Tri tri, int vert);

// Get the area of a triangle with a vector of verts and a trignale object
float getTriAreaFromVerts(const vector<Vert> &verts, const Tri &tri);

// Get the degeneracy value of a triangle with a vector of verts and a trignale object 
float triDegeneracyWithVerts(const vector<Vert> &verts, const Tri &tri);

// Erase a triangle with a given index from the triangle vector
void eraseTri(vector<Vert> &verts, vector<Tri> &tris, int i);

// Create a new triangle and insert it into a vector. Also assign the new 
// triangle as a parent of the triangles vertices
void insertNewTri(vector<Vert> &verts, vector<Tri> &tris, int a, int b, int c);

// Check if a given point lies inside a circumcircle of a triangle given the corners
bool pointInCircumcircle(Vector2 pt, Vector2 p1, Vector2 p2, Vector2 p3);

// Check if a point lies inside a triangle (a wrapper function for a raylib's implementation)
bool PointInTriangle(Vector2 pt, Vector2 v1, Vector2 v2, Vector2 v3);

// Get the area of a triangle given it's corners
float getTriArea(Vector2 a, Vector2 b, Vector2 c);

// Get the degeneracy value of a triangle given it's corners
float getTriDegeneracy(Vector2 a, Vector2 b, Vector2 c);

// Check if a given trio of vertices is in a clockwise order 
bool isClockwise(Vector2 a, Vector2 b, Vector2 c);

/**
 * === Pictures ===
 */


// Triangular texture object
struct TriTexture {
    Texture2D tex;
    int width, height;
    Vector2 corners[3] = {
        (Vector2){0, 0}, 
        (Vector2){0, 0}, 
        (Vector2){0, 0}
    };
};

// Get a triangular texture from a regular texture
TriTexture getTriTexture(const Texture2D &tex, Vector2 a, Vector2 b, Vector2 c);

// Draw a triangular texture
void drawTriTexture(const TriTexture &tex, Vector2 a, Vector2 b, Vector2 c);

// Draw a triangular texture padded
void drawTriTexturePadded(const TriTexture &tex, Vector2 a, Vector2 b, Vector2 c, int padding);


/**
 * === Utils ===
 */

// Draw a filled in triangle with a padding applied to the given points
void DrawTriangleFillPadded(Vector2 p1, Vector2 p2, Vector2 p3, int padding,
                            Color color);

// Draw an outline of a triangle with a padding applied to the given points
void DrawTriangleLinesPadded(Vector2 p1, Vector2 p2, Vector2 p3, int padding,
                             Color color);

// Draw an outline of a triangle with a padding and a stroke
void DrawTriangleLinesPadded(Vector2 p1, Vector2 p2, Vector2 p3, int padding,
                             int weight, Color color);

// Draw a triangle out of a texture
void DrawTriangleFromTexture(const Texture2D &texture, Vector2 *pos,
                             Vector2 *texcoords);

// Draw a padded triangle out of a texture
void DrawTriangleFromTexturePadded(const Texture2D &texture, Vector2 *pos,
                                   Vector2 *texcoords, int padding);

// Applie a padding to the given corners in-place
void paddTriangleInPlace(Vector2 &p1, Vector2 &p2, Vector2 &p3, int padding);

bool operator==(const Color &a, const Color &b);
std::ostream &operator<<(std::ostream &os, const Color &c);

// Vector operators
Vector2 operator-(const Vector2 &a, const Vector2 &b);
Vector2 operator-(const Vector2 &a);
Vector2 operator+(const Vector2 &a, const Vector2 &b);
float operator&(const Vector2 &a, const Vector2 &b);
float operator*(const Vector2 &a, const Vector2 &b);
Vector2 operator*(const Vector2 &a, const float &b);
Vector2 operator/(const Vector2 &a, const float &b);
Vector2 operator/(const Vector2 &a, const Vector2 &b);
std::ostream &operator<<(std::ostream &os, const Vector2 &v);

// Get a squared magnitude of a Vector2 
float magSq(const Vector2 &vec);

// Get a magnitude of a Vector2 (uses sqrt(), be wary)
float mag(const Vector2 &vec);

// Normalize Vector2 in-place and return a reference
Vector2 normalize(Vector2 &vec);

// Get a normalized version of a Vector2
Vector2 normalized(const Vector2 &vec);

// Random float functions
float randf();
float randf(float max);
float randf(float min, float max);

// push unique
template <typename T>
void pushUnique(vector<T> &vec, T &elt) {
    bool isin = false;

    for (T thing : vec) {
        if (thing == elt) {
            isin = true;
            break;
        }
    }

    if (!isin) vec.push_back(elt);
}

/**
 * === wfc ===
 */

// TODO: all of the stuff

struct WFCTriTileState {
    Color corners[3];
    int texture_index;
};

struct WFCTriTile {
    vector<int> states; // States
    vector<int> rots; // their rotations

    int neighbors[3];
    int backinds[3];
};

WFCTriTile createEmptyWFCTile(int possible_states);
WFCTriTileState generateWFCTriTileState(const vector<TriTexture> &textures, int texture_index);

bool statesCompatible(WFCTriTileState a, WFCTriTileState b, int ra, int rb);

void drawWFCTriTile(const vector<TriTexture> &textures, const vector<Vert> &verts, const vector<Tri> &tris, const vector<WFCTriTileState> &wfc_states, const vector<WFCTriTile> &wfc_tiles, int ind);

int WFC_collapseOne(vector<WFCTriTileState> &wfc_states, vector<WFCTriTile> &wfc_tiles);
void WFC_propagate(vector<WFCTriTileState> &wfc_states, vector<WFCTriTile> &wfc_tiles, int from);
void WFCStep(vector<WFCTriTileState> &wfc_states, vector<WFCTriTile> &wfc_tiles);


/**
 * === World ===
 */

enum WorldState {
    GENERATING_VERTS,
    TRIANGULATING,
    CLEANING_UP,
    SMOOTHING,
    DONE,
};

// A stupid variable, waiting to be deleted
const int TRIANGLE_MAX_COUNTER = 0;

// The main world class
class World {
   public:
    int w, h, vert_cols, vert_rows;

    char pressed_key = '_';

    int vertex_display_mode = 1;

    bool cutting = false;
    int cut_start = -1;

    vector<Vert> verts;
    vector<Tri> tris;

    int temporal_ind = 0;

    int target_edge_length;

    int state = WorldState::GENERATING_VERTS;

   public:
    World(){};
    void create(int w, int h, int vert_cols, int vert_rows);

    void draw(Texture2D trexture);
    void update(int frameCount, Vector2 mouse);
    void update(int frameCount);

    void onClick(Vector2 m);
    void keyGotPressed(char key);

    void smoothen(vector<int> verts, float amt);
    void smoothen(float amt);
    void triangulate(int i);

    void getNeighborVerts(int vert, vector<int> &inds);
};


/**
 * === Application ===
 */

class Application {
   public:
    // Technical definitions
    int width, height;
    int frameCount = 0;
    Application(){};
    bool setup(int _width, int _height);
    void setup();
    void start();
    void close();

    void update();
    void draw();

   public:
    // Cool definitions
    World wld;
    vector<TriTexture> textures;

    vector<Tri> tris;
    vector<Vert> verts;
    vector<WFCTriTileState> WFCStates;
    vector<WFCTriTile> WFCTiles;
};
