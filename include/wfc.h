#include <math.h>
#include <raylib.h>

#include <vector>

using std::vector;

struct TriTile {
    Vector2 a;
    Vector2 b;
    Vector2 c;
};

const TriTile TILE[6] = {
    {{0.5, 0.5}, {0, 0.5}, {0.25, 0}},  {{0.5, 0.5}, {0.25, 0}, {0.75, 0}},
    {{0.5, 0.5}, {0.75, 0}, {1, 0.5}},  {{0.5, 0.5}, {1, 0.5}, {0.75, 1}},
    {{0.5, 0.5}, {0.75, 1}, {0.25, 1}}, {{0.5, 0.5}, {0.25, 1}, {0, 0.5}},
};

vector<TriTile> getTritiles(int w, int h, int n);

void DrawTriTileFromTexture(Texture2D texture, Vector2 *pos, TriTile tile);
void DrawTriTileFromTexturePadded(const Texture2D &texture, Vector2 *pos,
                                  TriTile tile, int d);

struct WFCTileState {
    vector<int> allowed_neighbors[3];
};

struct WFCTile {
    int n[3];
    int nb[3];
    int rotation;
    vector<int> states;
};

class WFCField {
   public:
    vector<WFCTile> tiles;
    vector<WFCTile> states;

    vector<int> active;
    vector<int> uncollapsed;

   public:
    WFCField();

    void propagate();

    void updateActive();

    void collapseRandom();

    void step();

    bool tilesCompatible();
};
