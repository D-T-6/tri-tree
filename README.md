# Trage

A (not yet implemented) Model Synthesis demonstration on irregular triangular grids. Made with raylib.

## Build instructions

After you have cloned the repo, cd into it and run:

```
mkdir build
cd build
cmake ..
cd ..
cmake --build build
build/bin/trage
```
