#include <assert.h>
#include <math.h>

#include <algorithm>
#include <iostream>
#include <numeric>
#include <vector>

#include "trage.h"

using std::vector;

void World::create(int _w, int _h, int _vert_cols, int _vert_rows) {
    vert_cols = _vert_cols;
    vert_rows = _vert_rows;
    w = _w;
    h = _h;

    int w_to_use = w - 100, w_offset = 50;
    int h_to_use = w - 100, h_offset = 50;

    target_edge_length = w_to_use / vert_cols;

    for (int i = 0; i < vert_rows; i++) {
        for (int j = 0; j < vert_cols; j++) {
            createVert(verts,
                       1.0f * w_to_use / vert_cols *
                               (j + 0.5f + randf(-0.25f, 0.25f)) +
                           w_offset,
                       1.0f * h_to_use / vert_rows *
                               (i + 0.5f + randf(-0.25f, 0.25f)) +
                           h_offset);
        }
    }

    createVert(verts, 0, 0);
    createVert(verts, w, 0);
    createVert(verts, 0, h);
    createVert(verts, h, h);

    insertNewTri(verts, tris, vert_cols * vert_rows + 0,
                 vert_cols * vert_rows + 1, vert_cols * vert_rows + 2);
    insertNewTri(verts, tris, vert_cols * vert_rows + 1,
                 vert_cols * vert_rows + 2, vert_cols * vert_rows + 3);

    state = WorldState::TRIANGULATING;
}

void World::getNeighborVerts(int vert, vector<int> &inds) {
    inds.clear();

    for (int t : verts[vert].tris) {
        for (int x = 0; x < 3; x++) {
            bool add = tris[t][x] != vert;

            for (int i = 0; i < inds.size() && add; i++) {
                add = tris[t][x] != inds[i];
            }

            if (add) {
                inds.push_back(tris[t][x]);
            }
        }
    }
}