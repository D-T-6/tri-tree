#include <math.h>

#include <algorithm>
#include <iostream>
#include <vector>

#include "trage.h"

void World::triangulate(int i) {
    std::vector<int> vertinds;

    auto insvert = [&](int ind) {
        for (int v : vertinds) {
            if (ind == v || v == i) return;
        }

        vertinds.push_back(ind);
    };

    for (int j = tris.size() - 1; j >= 0; j--) {
        Tri tri = tris[j];

        if (pointInCircumcircle(verts[i], verts[tri.a], verts[tri.b],
                                verts[tri.c])) {
            insvert(tri.a);
            insvert(tri.b);
            insvert(tri.c);

            eraseTri(verts, tris, j);
        }
    }

    std::sort(vertinds.begin(), vertinds.end(), [=](int a, int b) {
        return atan2f((verts[a] - verts[i]).x, (verts[a] - verts[i]).y) <
               atan2f((verts[b] - verts[i]).x, (verts[b] - verts[i]).y);
    });

    for (int j = 0; j < vertinds.size(); j++) {
        insertNewTri(verts, tris, i, vertinds[j],
                     vertinds[(j + 1) % vertinds.size()]);
    }
}