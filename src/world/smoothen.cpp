#include <vector>

#include "trage.h"

void World::smoothen(float amt) {
    Vector2 adjustments[verts.size()];

    for (int i = 0; i < verts.size(); i++) {
        Vector2 target = {0.f, 0.f};

        std::vector<int> neigbors;

        getNeighborVerts(i, neigbors);

        for (int v : neigbors) {
            Vector2 iv = verts[v] - verts[i];

            target = target + (iv - normalized(iv) * target_edge_length);
        }

        adjustments[i] = target;
    }

    for (int i = 0; i < verts.size(); i++) {
        verts[i] = verts[i] + adjustments[i] * amt;
    }
}