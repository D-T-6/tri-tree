#include <string>
#include <vector>

#include "trage.h"

using std::vector;

void World::draw(Texture2D trexture) {
    // int i = 0;

    for (Tri tri : tris) {
        unsigned char a = 100;
        int p = 2;

        Color c = {255, 255, 255, a};

        if (tri.state == TriState::IS_BEING_CREATED) {
            a = 255.f * (TRIANGLE_MAX_COUNTER - tri.counter) /
                TRIANGLE_MAX_COUNTER;
            p = (6.f - p) * tri.counter / TRIANGLE_MAX_COUNTER + p;
            c = {70, 255, 200, a};
        }
        if (tri.state == TriState::IS_BEING_DELETED) {
            p = (20.f - p) * (TRIANGLE_MAX_COUNTER - tri.counter) /
                    TRIANGLE_MAX_COUNTER +
                p;
            a = 255.f * tri.counter / TRIANGLE_MAX_COUNTER;
            c = {200, 60, 40, a};
        }

        if (true) {
            // Vector2 points[4] = {
            //     verts[tri.b],
            //     verts[tri.a],
            //     verts[tri.c],
            // };

            // DrawTriTileFromTexturePadded(trexture, points, TILE[i], p);

            // i = (i + 1) % 6;
        }

        DrawTriangleLinesPadded(verts[tri.a], verts[tri.b], verts[tri.c], p, c);
    }

    for (int i = 0; i < verts.size(); i++) {
        DrawCircle(verts[i].x, verts[i].y, 5, {22, 22, 22, 180});
        DrawCircle(verts[i].x, verts[i].y, 3, {170, 170, 170, 255});
    }
}