#include <iostream>
#include <vector>

#include "trage.h"

void World::update(int frameCount) {
    bool busy = false;

    for (int i = tris.size() - 1; i >= 0; i--) {
        if (tris[i].state != IDLE) {
            busy = true;

            if (tris[i].counter > 0) {
                tris[i].counter--;
                continue;
            }

            if (tris[i].state == IS_BEING_DELETED) {
                eraseTri(verts, tris, i);
            } else {
                tris[i].state = IDLE;
            }
        }
    }

    if (busy) return;

    switch (state) {
        case TRIANGULATING:
            triangulate(temporal_ind);
            temporal_ind++;

            if (temporal_ind >= vert_cols * vert_rows) {
                state = CLEANING_UP;
            }

            break;

        case CLEANING_UP:
            for (int i = tris.size() - 1; i >= 0; i--) {
                if (tris[i].a >= verts.size() - 4 ||
                    tris[i].b >= verts.size() - 4 ||
                    tris[i].c >= verts.size() - 4 ||
                    triDegeneracyWithVerts(verts, tris[i]) < 0.15f)
                    eraseTri(verts, tris, i);
            }

            for (int i = 0; i < 4; i++) {
                eraseVert(verts, tris, verts.size() - 1);
            }

            state = SMOOTHING;
            temporal_ind = 30;

            break;

        case SMOOTHING:
            smoothen(0.2f);

            temporal_ind--;
            if (temporal_ind <= 0) {
                state = DONE;
            }

            break;
    }
}