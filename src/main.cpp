#include "trage.h"

using std::vector;

int main() {
    Application Trage;

    if (Trage.setup(600, 600)) Trage.start();
}