#include <iostream>
#include "trage.h"

void Application::setup() {
    wld.create(width, height, 8, 8);

    Texture2D texture = LoadTexture("assets/tri-textures.png");

    float s = 0.5f;
    for (float y = 0; y < 1.0f; y += s) {
        for (float x = 0; x < 1.0f; x += s) {
            TriTexture tex = getTriTexture(texture, (Vector2){x, y + s}, (Vector2){x + s / 2.0f, y}, (Vector2){x + s, y + s});

            textures.push_back(tex);
            WFCStates.push_back(generateWFCTriTileState(textures, textures.size() - 1));
        }
    }

    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            for (int p = 0; p < 3; p++) {
                for (int q = 0; q < 3; q++) {
                    // std::cout << i << " x " << j << " @ " << p << ", " << q << " = " << statesCompatible(WFCStates[i], WFCStates[j], p, q) << "\n"; 
                }
            }
        }
    }
    
    UnloadTexture(texture);
}

void Application::update() {
    wld.keyGotPressed(tolower(GetKeyPressed()));

    wld.update(frameCount, GetMousePosition());
}
void assignNeighborsAndBackinds(const vector<Tri> &tris, const vector<Vert> &verts, vector<WFCTriTile> &tiles);
void Application::draw() {
    ClearBackground({246, 246, 246, 255});

    if (wld.state != DONE) {
        tris = wld.tris;
        verts = wld.verts;
        int i = 0;

        for (Tri tri : tris) {
            unsigned char a = 100;
            int p = 2;

            Color c = {255, 255, 255, a};

            if (true) {
                Vector2 po[4] = {
                    verts[tri.b],
                    verts[tri.a],
                    verts[tri.c],
                };

                drawTriTexturePadded(textures[i], po[0], po[1], po[2], p);

                i = (i + 1) % 4;
            }

            DrawTriangleLinesPadded(verts[tri.a], verts[tri.b], verts[tri.c], p, c);
        }

        for (int i = 0; i < verts.size(); i++) {
            DrawCircle(verts[i].x, verts[i].y, 5, {22, 22, 22, 180});
            DrawCircle(verts[i].x, verts[i].y, 3, {170, 170, 170, 255});
        }
    } else {
        if (WFCTiles.size() == 0) {
            for (Tri tri : tris) {
                WFCTiles.push_back(createEmptyWFCTile(WFCStates.size()));
            }
            assignNeighborsAndBackinds(tris, verts, WFCTiles);
        }

        // if (!(frameCount % 30))
        WFCStep(WFCStates, WFCTiles);
    }

    for (int i = 0; i < WFCTiles.size(); i++) {
        drawWFCTriTile(textures, verts, tris, WFCStates, WFCTiles, i);
    }
}

void Application::close() {
    // UnloadTexture(trexture);
}

void assignNeighborsAndBackinds(const vector<Tri> &tris, const vector<Vert> &verts, vector<WFCTriTile> &tiles) {
    for (int i = 0; i < tiles.size(); i++) {
        Tri ti = tris[i];

        for (int tiv = 0; tiv < 3; tiv++) {
            for (int vt = 0; vt < verts[ti[tiv]].tris.size(); vt++) {
                if (triHasVert(tris[verts[ti[tiv]].tris[vt]], ti[(tiv + 1) % 3])) {
                    if (!triHasVert(tris[verts[ti[tiv]].tris[vt]], ti[(tiv + 2) % 3])) {
                        tiles[i].neighbors[tiv] = verts[ti[tiv]].tris[vt];
                        tiles[i].backinds[tiv] = 0;
                    }
                }
            }
        }
    }

    for (int i = 0; i < tiles.size(); i++) {
        WFCTriTile ti = tiles[i];

        for (int n = 0; n < 3; n++) {
            if (ti.neighbors[n] >= 0) {
                int ind = 0 * (tiles[ti.neighbors[n]].neighbors[0] == i) +
                            1 * (tiles[ti.neighbors[n]].neighbors[1] == i) +
                            2 * (tiles[ti.neighbors[n]].neighbors[2] == i);

                tiles[ti.neighbors[n]].backinds[ind] = n;
            }
        }
    }

    for (int i = 0; i < tiles.size(); i++) {
        std::cout << "#" << i << ":\n"
            << "  ns: " << tiles[i].neighbors[0] << ", "
                        << tiles[i].neighbors[1] << ", "
                        << tiles[i].neighbors[2] << "\n"
            << "  bis: " << tiles[i].backinds[0] << ", "
                         << tiles[i].backinds[1] << ", "
                         << tiles[i].backinds[2] << "\n";
    }
}