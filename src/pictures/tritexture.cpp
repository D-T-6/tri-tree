#include <raylib.h>
#include <math.h>

#include "trage.h"

TriTexture getTriTexture(const Texture2D &tex, Vector2 a, Vector2 b, Vector2 c) {
    TriTexture res;

    float x1 = fmin(a.x, fmin(b.x, c.x)),
        x2 = fmax(a.x, fmax(b.x, c.x)),
        y1 = fmin(a.y, fmin(b.y, c.y)),
        y2 = fmax(a.y, fmax(b.y, c.y));
    
    float ow = x2 - x1,
        oh = y2 - y1;

    if (isClockwise(a, b, c)) std::swap(b, c);
    
    res.corners[0] = (Vector2){(a.x - x1) / ow, (a.y - y1) / oh};
    res.corners[1] = (Vector2){(b.x - x1) / ow, (b.y - y1) / oh};
    res.corners[2] = (Vector2){(c.x - x1) / ow, (c.y - y1) / oh};

    res.width = ow * tex.width;
    res.height = oh * tex.height;

    Image img = LoadImageFromTexture(tex);
    ImageCrop(&img, {x1 * tex.width, y1 * tex.height, (float)res.width, (float)res.height});
    res.tex = LoadTextureFromImage(img);

    UnloadImage(img);

    return res;
}

void drawTriTexture(const TriTexture &tex, Vector2 a, Vector2 b, Vector2 c) {
    if (isClockwise(a, b, c)) std::swap(b, c);
    Vector2 pos1[3] = {a, b, c};
    Vector2 pos2[3] = {tex.corners[0], tex.corners[1], tex.corners[2]};
    DrawTriangleFromTexture(tex.tex, pos1, pos2);
}

void drawTriTexturePadded(const TriTexture &tex, Vector2 a, Vector2 b, Vector2 c, int padding) {
    paddTriangleInPlace(a, b, c, padding);
    drawTriTexture(tex, a, b, c);
}