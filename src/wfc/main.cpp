#include <vector>
#include <iostream>
#include <raylib.h>

#include "trage.h"

using std::vector;

WFCTriTile createEmptyWFCTile(int possible_states) {
    vector<int> states;
    vector<int> rotations;

    for (int i = 0; i < possible_states; i++) {
        for (int j = 0; j < 3; j++) {
            states.push_back(i);
            rotations.push_back(j);
        }
    }

    WFCTriTile t = {
        states,
        rotations
    };

    t.neighbors[0] = -1;
    t.neighbors[1] = -1;
    t.neighbors[2] = -1;

    t.backinds[0] = -1;
    t.backinds[1] = -1;
    t.backinds[2] = -1;

    return t;
}

WFCTriTileState generateWFCTriTileState(const vector<TriTexture> &textures, int ti) {
    Color corners[3];

    int x[3], y[3];

    Vector2 c = (textures[ti].corners[0] + textures[ti].corners[1] + textures[ti].corners[2]) / 3;
    c.x *= textures[ti].width;
    c.y *= textures[ti].height;

    for (int i = 0; i < 3; i++) {
        TriTexture t = textures[ti];
        Vector2 p = t.corners[i];

        p.x *= t.width;
        p.y *= t.height;

        p = p + normalized(c - p) * 2;

        x[i] = int(p.x);
        y[i] = int(p.y);
    }

    Image img = LoadImageFromTexture(textures[ti].tex);
    WFCTriTileState wtts;

    for (int i = 0; i < 3; i++) {
        wtts.corners[i] = GetImageColor(img, x[i], y[i]);
    }

    UnloadImage(img);

    wtts.texture_index = ti;

    return wtts;
}

