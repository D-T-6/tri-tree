#include <iostream>
#include <vector>

#include "trage.h"

using std::vector;

vector<TriTile> getTritiles(int w, int h, int n) {
    vector<TriTile> res;

    float cw = 1.0f / float(w);
    float ch = 1.0f / float(h);

    const Vector2 a = {0.0f * cw, ch}, b = {0.5f * cw, 0.0f},
                  c = {1.0f * cw, ch};

    for (int i = 0; i < n; i++) {
        Vector2 v = {(i % w) * cw, (i / w) * ch};

        res.push_back((TriTile){v + a, v + b, v + c});
    }

    return res;
}

void DrawTriTileFromTexture(Texture2D texture, Vector2 *pos, TriTile tile) {
    DrawTriTileFromTexturePadded(texture, pos, tile, 0);
}

void DrawTriTileFromTexturePadded(const Texture2D &texture, Vector2 *pos,
                                  TriTile tile, int d) {
    Vector2 tex_coords[4] = {
        tile.b,
        tile.a,
        tile.c,
    };

    DrawTriangleFromTexturePadded(texture, pos, tex_coords, d);
}

void WFCField::propagate() {
    while (active.size() > 0) {
        updateActive();
    }
}

void WFCField::updateActive() {
    vector<int> new_active;

    for (int i = active.size() - 1; i >= 0; i--) {
        int ind = active[i];
    }
}

void WFCField::collapseRandom() {}

void WFCField::step() {
    collapseRandom();
    propagate();
}