#include <vector>

#include <raylib.h>

#include "trage.h"

using std::vector;

void drawWFCTriTile(
    const vector<TriTexture> &textures,
    const vector<Vert> &verts, 
    const vector<Tri> &tris, 
    const vector<WFCTriTileState> &wfc_states, 
    const vector<WFCTriTile> &wfc_tiles, 
    int ind)
{

    Tri t = tris[ind];
    DrawTriangleLinesPadded(
        verts[t[0]], verts[t[1]], verts[t[2]],
        1, GRAY
    );

    int state_to_draw = rand() % wfc_tiles[ind].states.size();

    int rot = wfc_tiles[ind].states.size() > 0 ? wfc_tiles[ind].rots[state_to_draw] : 0;

    Vert a = verts[t[(rot + 0) % 3]];
    Vert b = verts[t[(rot + 1) % 3]];
    Vert c = verts[t[(rot + 2) % 3]];

    if (wfc_tiles[ind].states.size() > 0) {
        drawTriTexturePadded(
            textures[wfc_states[wfc_tiles[ind].states[state_to_draw]].texture_index],
            a, b, c,
            2
        );

        // DrawLineEx(C, verts[t[0]], 2.0f, BLACK);
        // DrawLineEx(C, (verts[t[rot]] + C) / 2, 2.0f, BLUE);
    }

    // Vector2 C = (a + b + c) / 3;
    
    // DrawText(std::to_string(wfc_tiles[ind].states.size()).c_str(), C.x, C.y - 10, 20, GREEN);
}