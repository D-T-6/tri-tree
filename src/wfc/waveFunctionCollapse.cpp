#include <iostream>
#include <vector>

#include <raylib.h>

#include "trage.h"

using std::vector;

bool statesCompatible(WFCTriTileState a, WFCTriTileState b, int ra, int rb) {
    ra = ((ra % 3) + 3) % 3;
    rb = ((rb % 3) + 3) % 3;

    Color ac[2] = {
        a.corners[((ra) + 0) % 3],
        a.corners[((ra) + 1) % 3]
    };

    Color bc[2] = {
        b.corners[((rb) + 0) % 3],
        b.corners[((rb) + 1) % 3]
    };

    std::cout << "comparing states  " << a.texture_index << " x "  << b.texture_index << " @ " << ra << ", " << rb << "\n";
    // std::cout << "  " << ac[0] << " \t " << ac[1] << "\n";
    // std::cout << "  " << bc[1] << " \t " << bc[0] << "\n";
    std::cout << "  they are " << (ac[0] == bc[1] && ac[1] == bc[0] ? "compatible" : "not compatible") << "\n";

    return ac[0] == bc[1] && ac[1] == bc[0];
}

int WFC_collapseOne(vector<WFCTriTileState> &states, vector<WFCTriTile> &tiles) {
    int rec = tiles.size() * 10;
    int ind = -1;

    for (int i = 0; i < tiles.size(); i++) {
        int s = tiles[i].states.size();
        if (s > 1 && s < rec) {
            rec = s;
            ind = i;
        }
    }

    if (ind >= 0) {
        int state = tiles[ind].states[rand() % tiles[ind].states.size()];
        int rot = tiles[ind].rots[rand() % tiles[ind].rots.size()];

        tiles[ind].states.clear();
        tiles[ind].rots.clear();

        tiles[ind].states.push_back(state);
        tiles[ind].rots.push_back(rot);
    }

    return ind;
}

void WFC_propagate(vector<WFCTriTileState> &states, vector<WFCTriTile> &tiles, int from) {
    vector<int> actives = {from};

    while (actives.size() > 0) {
        vector<int> nactives;

        for (int i = 0; i < actives.size(); i++) {
            int mi = actives[i];

            for (int n = 0; n < 3; n++) {
                int ni = tiles[mi].neighbors[n];

                if (ni >= 0)
                for (int nsi = tiles[ni].states.size() - 1; nsi >= 0; nsi--) {

                    bool ok = false;
                    for (int nn = 0; nn < 3; nn++) {
                        int nni = tiles[ni].neighbors[nni];

                        if (nni >= 0) {
                            for (int nnsi = 0; nnsi < tiles[nni].states.size(); nnsi++) {
                                ok |= statesCompatible(
                                    states[tiles[ni].states[nsi]], 
                                    states[tiles[nni].states[nnsi]],
                                    -tiles[ni].rots[nsi] + nni,
                                    2-tiles[nni].rots[nnsi] + tiles[ni].backinds[nn]
                                );
                            }
                        }
                    }

                    if (!ok) {
                        tiles[ni].states.erase(tiles[ni].states.begin() + nsi);
                        tiles[ni].rots.erase(tiles[ni].rots.begin() + nsi);
                        pushUnique(nactives, ni);
                    }
                }
            }
        }

        actives = nactives;
    }
}

void WFCStep(vector<WFCTriTileState> &states, vector<WFCTriTile> &tiles) {
    int collapsed = WFC_collapseOne(states, tiles);
    if (collapsed >= 0)
        WFC_propagate(states, tiles, collapsed);
}